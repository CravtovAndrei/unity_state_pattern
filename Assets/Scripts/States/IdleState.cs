﻿using UnityEngine;

public class IdleState : State 
{
    public override void Handle1()
    {
        Debug.Log("Idle State handles Request 1");
        Debug.Log("Idle State changes the state of the context");
        this._context.TransitionTo<WorkingState>();
    }

    public override void Handle2()
    {
        Debug.Log("Idle State handles Request 2");
    }
}
