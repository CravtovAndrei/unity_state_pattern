﻿using UnityEngine;

public class WorkingState : State 
{
    public override void Handle1()
    {
        Debug.Log("Working State handles request 1");
    }

    public override void Handle2()
    {
        Debug.Log("Working State handles request 2");
        Debug.Log("Working State changes the state of the context");
        this._context.TransitionTo<IdleState>();
    }
}
