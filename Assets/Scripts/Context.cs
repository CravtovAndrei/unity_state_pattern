﻿using UnityEngine;

public class Context : MonoBehaviour
{
    State _state = null;

    void Awake()
    {
        InitializeContext(); 
        Request1();
        Request2();
    }

    public void InitializeContext()
    {
        this.TransitionTo<IdleState>();
    }

    public void TransitionTo<T>() where T: State
    {
        if(_state)
            Destroy(_state);
        
        _state =  gameObject.AddComponent<T>();
        
        Debug.Log($"Context: Transition to {_state.GetType().Name}");
        this._state.SetContext(this);
    }

    public void Request1()
    {
        this._state.Handle1();
    }
    
    public void Request2()
    {
        this._state.Handle2();
    }
}
